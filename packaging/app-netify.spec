
Name: app-netify
Epoch: 1
Version: 3.0.0
Release: 1%{dist}
Summary: Netify Agent
License: GPLv3
Group: Applications/Apps
Packager: Netify
Vendor: Netify
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Netify provides full transparency into what's happening on your network.  Features include: device discovery, network intelligence, bandwidth reporting, application detection, protocol analysis, network forensics, and more.

%package core
Summary: Netify Agent - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-base-core >= 1:2.5.25
Requires: app-network-core >= 1:2.5.0
Requires: netifyd >= 3.00
Conflicts: app-netify-fwa-core < 1:2.6.0

%description core
Netify provides full transparency into what's happening on your network.  Features include: device discovery, network intelligence, bandwidth reporting, application detection, protocol analysis, network forensics, and more.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/netify
cp -r * %{buildroot}/usr/clearos/apps/netify/

install -d -m 0755 %{buildroot}/var/clearos/netify
install -d -m 0755 %{buildroot}/var/clearos/netify/backup
install -D -m 0644 packaging/netifyd.php %{buildroot}/var/clearos/base/daemon/netifyd.php
install -D -m 0755 packaging/network-configuration-event %{buildroot}/var/clearos/events/network_configuration/netify

%post
logger -p local6.notice -t installer 'app-netify - installing'

%post core
logger -p local6.notice -t installer 'app-netify-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/netify/deploy/install ] && /usr/clearos/apps/netify/deploy/install
fi

[ -x /usr/clearos/apps/netify/deploy/upgrade ] && /usr/clearos/apps/netify/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-netify - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-netify-api - uninstalling'
    [ -x /usr/clearos/apps/netify/deploy/uninstall ] && /usr/clearos/apps/netify/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/netify/controllers
/usr/clearos/apps/netify/htdocs
/usr/clearos/apps/netify/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/netify/packaging
%exclude /usr/clearos/apps/netify/unify.json
%dir /usr/clearos/apps/netify
%dir /var/clearos/netify
%dir /var/clearos/netify/backup
/usr/clearos/apps/netify/deploy
/usr/clearos/apps/netify/language
/usr/clearos/apps/netify/api
/usr/clearos/apps/netify/libraries
/var/clearos/base/daemon/netifyd.php
/var/clearos/events/network_configuration/netify
