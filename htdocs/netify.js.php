<?php

/**
 * Netify ajax helpers.
 *
 * @category   apps
 * @package    netify
 * @subpackage javascript
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('netify');

///////////////////////////////////////////////////////////////////////////////
// J A V A S C R I P T
///////////////////////////////////////////////////////////////////////////////

// TODO: move this to framework
$api_info = file_get_contents('/etc/api.conf');
$api_key = trim(preg_replace('/.*=\s*/', '', $api_info));

header('Content-Type:application/x-javascript');
?>

///////////////////////////////////////////////////////////////////////////
// M A I N
///////////////////////////////////////////////////////////////////////////

$(document).ready(function() {

    // Translations
    //-------------

    lang_dashboard = '<?php echo lang("netify_netify_dashboard"); ?>';
    lang_provisioned = '<?php echo lang("netify_provisioned"); ?>';
    lang_unprovisioned = '<?php echo lang("netify_unprovisioned"); ?>';
    lang_provision_agent = '<?php echo lang("netify_provision_netify_agent"); ?>';
    api_key = '<?php echo $api_key ?>';

    $("#uuid_text").html(clearos_loading());
    $("#provision_status_text").html(clearos_loading());

    $('#provision_button').click(function(e) {
        // Reload in order to reset ajax counter
        window.location = '/app/netify';
    });

    // Provision widget
    //-----------------

    if ($("#netify_sink_enabled").length != 0)
        netifyProvisionStatus(0);
});

///////////////////////////////////////////////////////////////////////////
// F U N C T I O N S
///////////////////////////////////////////////////////////////////////////

function netifyProvisionStatus(count) {
    count++;

    $.ajax({
        url: '/api/v1/netify/agent', 
        method: 'GET',
        dataType: 'json',
        headers: {
            'x-api-key': api_key
        },
        success : function(payload) {
            netifyShowProvisionStatus(payload, count);

            // No need to re-fetch live status if provisioned
            if (!((payload.status_code == 0) && (payload.data.status == 'provisioned'))) {
                if (count < 1000) {
                    window.setTimeout(netifyProvisionStatus, 3000, count);
                } else {
                    $("#provision_status_text").html('Idle');
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            window.setTimeout(netifyProvisionStatus, 3000, count);
        }
    });
}

function netifyShowProvisionStatus(payload, count) {
    if (payload.status_code == 0) {
        sinkState = payload.data.sink_state;
        status = payload.data.status;
        uuid = payload.data.uuid;

        if (sinkState) {
            if (status == 'provisioned') {
                button_text = lang_dashboard;
                button_url = 'https://portal.netify.ai/app';
                status_text = '<span class=theme-text-good-status>' + lang_provisioned + '</span>';
            } else if (status == 'unprovisioned') {
                button_text = lang_provision_agent;
                button_url = 'https://portal.netify.ai/manager/agents/create?provision_code=' + uuid;
                status_text = '<span class=theme-text-bad-status>' + lang_unprovisioned + '</span>';
            } else if (status == 'missing') {
                button_text = '';
                status_text = '<span class=theme-text-bad-status>' + 'Connecting...' + '</span>';
            } else {
                button_text = '';
                status_text = '<span class=theme-text-bad-status>' + 'Unable to connect' + '</span>';
            }

            $("#uuid_text").html(uuid);
            $("#provision_status_text").html(status_text);

            if (button_text != '') {
                $("#provision_button").html(button_text);
                $("#provision_button").prop('href', button_url);
                $("#provision_button").removeClass('hide');
            } else {
                $("#provision_button").addClass('hide');
            }
        }

    }
}

// vim: syntax=javascript
