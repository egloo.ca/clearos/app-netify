<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'netify';
$app['version'] = '3.0.0';
$app['vendor'] = 'Netify';
$app['packager'] = 'Netify';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('netify_app_description');
$app['documentation_url'] = 'https://www.netify.ai/resources';
$app['tooltip'] = [
    sprintf(lang('netify_tooltip_account_required'), "<a href=\"https://portal.netify.ai/register\" target=\"_blank\">", "</a>"),
    lang('netify_tooltip_metadata'),
    lang('netify_tooltip_data')
];
$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'Netify',
        'url' => 'https://www.netify.ai',
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('netify_app_name');
$app['category'] = lang('base_category_cloud');
$app['subcategory'] = lang('base_subcategory_services');

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['netify']['title'] = $app['name'];
$app['controllers']['provision']['title'] = lang('netify_provision');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-base-core >= 1:2.5.25',
    'app-network-core >= 1:2.5.0',
    'netifyd >= 3.00',
);

$app['core_conflicts'] = array(
    'app-netify-fwa-core < 1:2.6.0'
);

$app['core_directory_manifest'] = array(
   '/var/clearos/netify' => array(),
   '/var/clearos/netify/backup' => array(),
);

$app['core_file_manifest'] = array(
    'netifyd.php'=> array('target' => '/var/clearos/base/daemon/netifyd.php'),
    'network-configuration-event'=> array(
        'target' => '/var/clearos/events/network_configuration/netify',
        'mode' => '0755'
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App Removal Dependencies
/////////////////////////////////////////////////////////////////////////////

$app['delete_dependency'] = array(
);
