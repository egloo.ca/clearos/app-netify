<?php

$netify_protocols = array(
    '1' => array(
        'name' => 'FTP Control',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/ftp-control?ref=clearos',
    ),
    '2' => array(
        'name' => 'POP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/pop-mail?ref=clearos',
    ),
    '3' => array(
        'name' => 'SMTP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/smtp-mail?ref=clearos',
    ),
    '4' => array(
        'name' => 'IMAP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/imap-mail?ref=clearos',
    ),
    '5' => array(
        'name' => 'DNS',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/dns?ref=clearos',
    ),
    '6' => array(
        'name' => 'Internet Printing',
        'category' => 'Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/ipp?ref=clearos',
    ),
    '7' => array(
        'name' => 'HTTP',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/http?ref=clearos',
    ),
    '8' => array(
        'name' => 'Multicast DNS',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/mdns?ref=clearos',
    ),
    '9' => array(
        'name' => 'Network Time Protocol',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/ntp?ref=clearos',
    ),
    '10' => array(
        'name' => 'Windows Networking',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/netbios?ref=clearos',
    ),
    '11' => array(
        'name' => 'NFS',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/nfs?ref=clearos',
    ),
    '12' => array(
        'name' => 'SSDP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/ssdp?ref=clearos',
    ),
    '13' => array(
        'name' => 'BGP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/bgp?ref=clearos',
    ),
    '14' => array(
        'name' => 'SNMP',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/snmp?ref=clearos',
    ),
    '15' => array(
        'name' => 'X-Windows Display',
        'category' => 'Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/xdmcp?ref=clearos',
    ),
    '16' => array(
        'name' => 'Windows SMB v1',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/smb?ref=clearos',
    ),
    '17' => array(
        'name' => 'System Logger',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/syslog?ref=clearos',
    ),
    '18' => array(
        'name' => 'DHCP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/dhcp?ref=clearos',
    ),
    '19' => array(
        'name' => 'Postgres',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/postgresql?ref=clearos',
    ),
    '20' => array(
        'name' => 'MySQL',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/mysql?ref=clearos',
    ),
    '22' => array(
        'name' => 'Direct Download Link',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/direct-download-link?ref=clearos',
    ),
    '23' => array(
        'name' => 'Secure POP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/pops?ref=clearos',
    ),
    '24' => array(
        'name' => 'AppleJuice',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/applejuice?ref=clearos',
    ),
    '25' => array(
        'name' => 'Direct Connect',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/direct-connect?ref=clearos',
    ),
    '27' => array(
        'name' => 'CoAP',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/coap?ref=clearos',
    ),
    '28' => array(
        'name' => 'VMware',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/vmware?ref=clearos',
    ),
    '29' => array(
        'name' => 'Secure SMTP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/smtps?ref=clearos',
    ),
    '30' => array(
        'name' => 'Facebook Zero',
        'category' => 'Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/facebook-zero?ref=clearos',
    ),
    '31' => array(
        'name' => 'Ubiquiti AirControl 2',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/ubiquiti-aircontrol-2?ref=clearos',
    ),
    '32' => array(
        'name' => 'Kontiki',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/kontiki?ref=clearos',
    ),
    '33' => array(
        'name' => 'OpenFT',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/openft?ref=clearos',
    ),
    '34' => array(
        'name' => 'FastTrack',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/fasttrack?ref=clearos',
    ),
    '35' => array(
        'name' => 'Gnutella',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/gnutella?ref=clearos',
    ),
    '36' => array(
        'name' => 'eDonkey',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/edonkey?ref=clearos',
    ),
    '37' => array(
        'name' => 'BitTorrent',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/bittorrent?ref=clearos',
    ),
    '38' => array(
        'name' => 'Skype Outbound',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/skype-outboud?ref=clearos',
    ),
    '40' => array(
        'name' => 'Memcached',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/memcached?ref=clearos',
    ),
    '41' => array(
        'name' => 'Windows SMB v2/v3',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/smbv23?ref=clearos',
    ),
    '42' => array(
        'name' => 'Mining',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/mining?ref=clearos',
    ),
    '43' => array(
        'name' => 'Nest Protect',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/nest-protect?ref=clearos',
    ),
    '44' => array(
        'name' => 'Modbus',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/modbus?ref=clearos',
    ),
    '45' => array(
        'name' => 'WhatsApp Video',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/whatsapp-video?ref=clearos',
    ),
    '47' => array(
        'name' => 'Xbox',
        'category' => 'Game Providers',
        'url' => 'https://www.netify.ai/resources/protocols/xbox?ref=clearos',
    ),
    '48' => array(
        'name' => 'QQ Instant Messenger',
        'category' => 'Instant Messaging',
        'url' => 'https://www.netify.ai/resources/protocols/tencent-qq?ref=clearos',
    ),
    '50' => array(
        'name' => 'Real Time Streaming Protocol',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/rtsp?ref=clearos',
    ),
    '51' => array(
        'name' => 'Secure IMAP Mail',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/imaps?ref=clearos',
    ),
    '52' => array(
        'name' => 'Icecast',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/icecast?ref=clearos',
    ),
    '53' => array(
        'name' => 'PPLive',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/pplive?ref=clearos',
    ),
    '54' => array(
        'name' => 'PPStream',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/ppstream?ref=clearos',
    ),
    '55' => array(
        'name' => 'Zattoo',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/zattoo?ref=clearos',
    ),
    '56' => array(
        'name' => 'SHOUTcast',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/shoutcast?ref=clearos',
    ),
    '57' => array(
        'name' => 'SopCast',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/sopcast?ref=clearos',
    ),
    '58' => array(
        'name' => 'TvAnts',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/tvants?ref=clearos',
    ),
    '59' => array(
        'name' => 'TVUPlayer',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/tvu-tv?ref=clearos',
    ),
    '60' => array(
        'name' => 'HTTP Download',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/http-download?ref=clearos',
    ),
    '61' => array(
        'name' => 'QQLive',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/qqlive?ref=clearos',
    ),
    '62' => array(
        'name' => 'Thunder',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/thunder?ref=clearos',
    ),
    '63' => array(
        'name' => 'Soulseek',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/soulseek?ref=clearos',
    ),
    '64' => array(
        'name' => 'Certificateless SSL',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/certificateless-ssl?ref=clearos',
    ),
    '65' => array(
        'name' => 'IRC',
        'category' => 'Instant Messaging',
        'url' => 'https://www.netify.ai/resources/protocols/irc?ref=clearos',
    ),
    '66' => array(
        'name' => 'Anything In Anything',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/anything-in-anything?ref=clearos',
    ),
    '67' => array(
        'name' => 'XMPP',
        'category' => 'Instant Messaging',
        'url' => 'https://www.netify.ai/resources/protocols/jabber?ref=clearos',
    ),
    '69' => array(
        'name' => 'AOL Instant Messenger',
        'category' => 'Instant Messaging',
        'url' => 'https://www.netify.ai/resources/protocols/aol-im?ref=clearos',
    ),
    '71' => array(
        'name' => 'BattleField',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/battlefield?ref=clearos',
    ),
    '73' => array(
        'name' => 'VRRP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/vrrp?ref=clearos',
    ),
    '74' => array(
        'name' => 'Steam',
        'category' => 'Game Providers',
        'url' => 'https://www.netify.ai/resources/protocols/steam?ref=clearos',
    ),
    '75' => array(
        'name' => 'Half-Life 2',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/half-life-2?ref=clearos',
    ),
    '76' => array(
        'name' => 'World of Warcraft',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/world-of-warcraft?ref=clearos',
    ),
    '77' => array(
        'name' => 'Telnet',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/telnet?ref=clearos',
    ),
    '78' => array(
        'name' => 'STUN',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/stun?ref=clearos',
    ),
    '79' => array(
        'name' => 'IPsec',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/ipsec?ref=clearos',
    ),
    '80' => array(
        'name' => 'GRE',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/gre?ref=clearos',
    ),
    '81' => array(
        'name' => 'ICMP - Ping',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/icmp?ref=clearos',
    ),
    '82' => array(
        'name' => 'IGMP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/igmp?ref=clearos',
    ),
    '83' => array(
        'name' => 'EGP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/egp?ref=clearos',
    ),
    '84' => array(
        'name' => 'SCTP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/sctp?ref=clearos',
    ),
    '85' => array(
        'name' => 'OSPF',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/ospf?ref=clearos',
    ),
    '86' => array(
        'name' => 'IP in IP',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/ip-in-ip?ref=clearos',
    ),
    '87' => array(
        'name' => 'RTP',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/rtp?ref=clearos',
    ),
    '88' => array(
        'name' => 'Remote Desktop Protocol',
        'category' => 'Remote Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/rdp?ref=clearos',
    ),
    '89' => array(
        'name' => 'VNC',
        'category' => 'Remote Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/vnc?ref=clearos',
    ),
    '90' => array(
        'name' => 'pcAnywhere',
        'category' => 'Remote Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/pcanywhere?ref=clearos',
    ),
    '91' => array(
        'name' => 'SSL',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/ssl?ref=clearos',
    ),
    '92' => array(
        'name' => 'SSH Secure Shell',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/ssh?ref=clearos',
    ),
    '93' => array(
        'name' => 'Usenet',
        'category' => 'Forums',
        'url' => 'https://www.netify.ai/resources/protocols/usenet?ref=clearos',
    ),
    '94' => array(
        'name' => 'MGCP',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/mgcp?ref=clearos',
    ),
    '95' => array(
        'name' => 'Asterisk IAX',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/iax?ref=clearos',
    ),
    '96' => array(
        'name' => 'TFTP',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/tftp?ref=clearos',
    ),
    '97' => array(
        'name' => 'Apple File Services',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/afp?ref=clearos',
    ),
    '98' => array(
        'name' => 'Stealthnet',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/stealthnet?ref=clearos',
    ),
    '99' => array(
        'name' => 'Aimini',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/aimini?ref=clearos',
    ),
    '100' => array(
        'name' => 'SIP',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/sip?ref=clearos',
    ),
    '101' => array(
        'name' => 'Truphone',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/truphone?ref=clearos',
    ),
    '102' => array(
        'name' => 'ICMP - Ping IPv6',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/icmp-v6?ref=clearos',
    ),
    '103' => array(
        'name' => 'DHCP IPv6',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/dhcp-v6?ref=clearos',
    ),
    '104' => array(
        'name' => 'Armagetron',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/armagetron?ref=clearos',
    ),
    '105' => array(
        'name' => 'Crossfire',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/crossfire?ref=clearos',
    ),
    '106' => array(
        'name' => 'Dofus',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/dofus?ref=clearos',
    ),
    '107' => array(
        'name' => 'Fiesta',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/fiesta?ref=clearos',
    ),
    '108' => array(
        'name' => 'Florensia',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/florensia?ref=clearos',
    ),
    '109' => array(
        'name' => 'Guild Wars',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/guild-wars?ref=clearos',
    ),
    '110' => array(
        'name' => 'ActiveSync',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/activesync?ref=clearos',
    ),
    '111' => array(
        'name' => 'Kerberos',
        'category' => 'Authentication',
        'url' => 'https://www.netify.ai/resources/protocols/kerberos?ref=clearos',
    ),
    '112' => array(
        'name' => 'LDAP',
        'category' => 'Authentication',
        'url' => 'https://www.netify.ai/resources/protocols/ldap?ref=clearos',
    ),
    '113' => array(
        'name' => 'MapleStory',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/maplestory?ref=clearos',
    ),
    '114' => array(
        'name' => 'Microsoft SQL Server',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/microsoft-sql-server?ref=clearos',
    ),
    '115' => array(
        'name' => 'PPTP',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/pptp?ref=clearos',
    ),
    '116' => array(
        'name' => 'Warcraft III',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/warcraft-iii?ref=clearos',
    ),
    '117' => array(
        'name' => 'World of Kung Fu',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/world-of-kung-fu?ref=clearos',
    ),
    '121' => array(
        'name' => 'Dropbox',
        'category' => 'File Sharing',
        'url' => 'https://www.netify.ai/resources/protocols/dropbox?ref=clearos',
    ),
    '125' => array(
        'name' => 'Skype',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/skype?ref=clearos',
    ),
    '127' => array(
        'name' => 'DCE/RPC',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/dce-rpc?ref=clearos',
    ),
    '128' => array(
        'name' => 'NetFlow',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/netflow?ref=clearos',
    ),
    '129' => array(
        'name' => 'sFlow',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/sflow?ref=clearos',
    ),
    '130' => array(
        'name' => 'HTTP Web Proxy Connect',
        'category' => 'Proxy',
        'url' => 'https://www.netify.ai/resources/protocols/http-connect?ref=clearos',
    ),
    '131' => array(
        'name' => 'HTTP Web Proxy',
        'category' => 'Proxy',
        'url' => 'https://www.netify.ai/resources/protocols/http-proxy?ref=clearos',
    ),
    '132' => array(
        'name' => 'Citrix Thin Client',
        'category' => 'Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/citrix?ref=clearos',
    ),
    '138' => array(
        'name' => 'Check_MK',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/check-mk?ref=clearos',
    ),
    '139' => array(
        'name' => 'Apache JServ',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/ajp?ref=clearos',
    ),
    '142' => array(
        'name' => 'WhatsApp',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/whatsapp-ssl?ref=clearos',
    ),
    '146' => array(
        'name' => 'RADIUS',
        'category' => 'Authentication',
        'url' => 'https://www.netify.ai/resources/protocols/radius?ref=clearos',
    ),
    '148' => array(
        'name' => 'TeamViewer',
        'category' => 'Remote Desktop',
        'url' => 'https://www.netify.ai/resources/protocols/teamviewer?ref=clearos',
    ),
    '150' => array(
        'name' => 'Lotus Notes',
        'category' => 'Mail',
        'url' => 'https://www.netify.ai/resources/protocols/lotus-notes?ref=clearos',
    ),
    '151' => array(
        'name' => 'SAP',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/sap?ref=clearos',
    ),
    '152' => array(
        'name' => 'GPRS Tunneling',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/gprs?ref=clearos',
    ),
    '153' => array(
        'name' => 'uPnP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/upnp?ref=clearos',
    ),
    '154' => array(
        'name' => 'Link-Local Multicast Name Resolution',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/llmnr?ref=clearos',
    ),
    '155' => array(
        'name' => 'RemoteScan',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/remotescan?ref=clearos',
    ),
    '156' => array(
        'name' => 'Spotify Synchronization',
        'category' => 'Media Provider',
        'url' => 'https://www.netify.ai/resources/protocols/spotify-synchronization-protocol?ref=clearos',
    ),
    '158' => array(
        'name' => 'H.323',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/h323?ref=clearos',
    ),
    '159' => array(
        'name' => 'OpenVPN',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/openvpn?ref=clearos',
    ),
    '160' => array(
        'name' => 'New Office Environment',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/new-office-environment?ref=clearos',
    ),
    '161' => array(
        'name' => 'Cisco VPN',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/ciscovpn?ref=clearos',
    ),
    '162' => array(
        'name' => 'TeamSpeak',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/teamspeak?ref=clearos',
    ),
    '164' => array(
        'name' => 'Skinny',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/cisco-skinny?ref=clearos',
    ),
    '165' => array(
        'name' => 'RTP Control Protocol',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/rtcp?ref=clearos',
    ),
    '166' => array(
        'name' => 'Rsync',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/rsync?ref=clearos',
    ),
    '167' => array(
        'name' => 'Oracle',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/oracle?ref=clearos',
    ),
    '168' => array(
        'name' => 'Corba',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/corba?ref=clearos',
    ),
    '170' => array(
        'name' => 'Whois',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/whois?ref=clearos',
    ),
    '171' => array(
        'name' => 'Collectd',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/collectd?ref=clearos',
    ),
    '172' => array(
        'name' => 'SOCKS5',
        'category' => 'Proxy',
        'url' => 'https://www.netify.ai/resources/protocols/socks-5?ref=clearos',
    ),
    '173' => array(
        'name' => 'Nintendo',
        'category' => 'Game Providers',
        'url' => 'https://www.netify.ai/resources/protocols/nintendo?ref=clearos',
    ),
    '174' => array(
        'name' => 'Real Time Messaging',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/rtmp?ref=clearos',
    ),
    '175' => array(
        'name' => 'FTP Data',
        'category' => 'File Server',
        'url' => 'https://www.netify.ai/resources/protocols/ftp-data?ref=clearos',
    ),
    '177' => array(
        'name' => 'ZeroMQ',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/zeromq?ref=clearos',
    ),
    '181' => array(
        'name' => 'H.248 MGCP',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/gateway-control-protocol?ref=clearos',
    ),
    '182' => array(
        'name' => 'Redis',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/redis?ref=clearos',
    ),
    '183' => array(
        'name' => 'Pando Media Booster',
        'category' => 'Game Providers',
        'url' => 'https://www.netify.ai/resources/protocols/pando-media-booster?ref=clearos',
    ),
    '184' => array(
        'name' => 'VHUA',
        'category' => 'Database',
        'url' => 'https://www.netify.ai/resources/protocols/vhua?ref=clearos',
    ),
    '185' => array(
        'name' => 'Telegram',
        'category' => 'Instant Messaging',
        'url' => 'https://www.netify.ai/resources/protocols/telegram?ref=clearos',
    ),
    '188' => array(
        'name' => 'QUIC',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/quic?ref=clearos',
    ),
    '189' => array(
        'name' => 'WhatsApp Voice',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/whatsapp?ref=clearos',
    ),
    '191' => array(
        'name' => 'Ookla Speedtest',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/ookla-speedtest?ref=clearos',
    ),
    '192' => array(
        'name' => 'AMQP',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/amqp?ref=clearos',
    ),
    '194' => array(
        'name' => 'KakaoTalk Voice',
        'category' => 'Video Conferencing',
        'url' => 'https://www.netify.ai/resources/protocols/kakaotalk-voice?ref=clearos',
    ),
    '196' => array(
        'name' => 'HTTPS',
        'category' => 'Web',
        'url' => 'https://www.netify.ai/resources/protocols/https?ref=clearos',
    ),
    '198' => array(
        'name' => 'MPEG Transport Stream',
        'category' => 'Media',
        'url' => 'https://www.netify.ai/resources/protocols/mpeg-ts?ref=clearos',
    ),
    '201' => array(
        'name' => 'Google Hangout',
        'category' => 'Video Conferencing',
        'url' => 'https://www.netify.ai/resources/protocols/google-hangout?ref=clearos',
    ),
    '204' => array(
        'name' => 'Canon BJNP',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/canon-bjnp?ref=clearos',
    ),
    '206' => array(
        'name' => 'WireGuard',
        'category' => 'VPN',
        'url' => 'https://www.netify.ai/resources/protocols/wireguard?ref=clearos',
    ),
    '207' => array(
        'name' => 'SMPP',
        'category' => 'VoIP and Telephony',
        'url' => 'https://www.netify.ai/resources/protocols/smpp?ref=clearos',
    ),
    '209' => array(
        'name' => 'Tinc',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/tinc?ref=clearos',
    ),
    '213' => array(
        'name' => 'StarCraft',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/starcraft?ref=clearos',
    ),
    '214' => array(
        'name' => 'Teredo IPv6',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/teredo?ref=clearos',
    ),
    '216' => array(
        'name' => 'EEP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/eep-hep?ref=clearos',
    ),
    '222' => array(
        'name' => 'MQ Telemetry Transport',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/mqtt?ref=clearos',
    ),
    '223' => array(
        'name' => 'Rx',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/rx?ref=clearos',
    ),
    '226' => array(
        'name' => 'Git',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/git?ref=clearos',
    ),
    '227' => array(
        'name' => 'DRDA',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/drda?ref=clearos',
    ),
    '229' => array(
        'name' => 'SOME/IP',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/someip?ref=clearos',
    ),
    '230' => array(
        'name' => 'FIX',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/fix?ref=clearos',
    ),
    '235' => array(
        'name' => 'CSGO',
        'category' => 'Games',
        'url' => 'https://www.netify.ai/resources/protocols/csgo?ref=clearos',
    ),
    '236' => array(
        'name' => 'LISP',
        'category' => 'Networking',
        'url' => 'https://www.netify.ai/resources/protocols/lisp?ref=clearos',
    ),
    '237' => array(
        'name' => 'Diameter',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/diameter?ref=clearos',
    ),
    '238' => array(
        'name' => 'Apple Push',
        'category' => 'Infrastructure',
        'url' => 'https://www.netify.ai/resources/protocols/apple-push?ref=clearos',
    ),
);
