<?php

/**
 * Netifyd class.
 *
 * @category   apps
 * @package    netify
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\netify;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('netify');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Rest_Client as Rest_Client;

clearos_load_library('base/Daemon');
clearos_load_library('base/File');
clearos_load_library('base/Rest_Client');

// Exceptions
//-----------

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netifyd class.
 *
 * @category   apps
 * @package    netify
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

class Netifyd extends Daemon
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/netifyd.conf';
    const FILE_CACHE = '/var/clearos/framework/cache/netifyd.conf';
    const FILE_UUID = '/etc/netify.d/agent.uuid';

    const STATUS_PROVISIONED = 'provisioned';      // Provisioned
    const STATUS_UNPROVISIONED = 'unprovisioned';  // Unprovisioned
    const STATUS_OFFLINE = 'offline';              // Unable do connect to API servers, e.g. DNS issue
    const STATUS_MISSING = 'missing';              // Missing from sink agents listing

    protected $api_servers = ['https://manager.netify.ai'];

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $config = array();
    protected $uuid = '';

    /**
     * Netifyd constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct('netifyd');
    }

    /**
     * Delete a MAC from whitelist.
     *
     * @param string $mac MAC address
     *
     * @return void
     * @throws Engine_Exception
     */

    public function delete_mac($mac)
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_CONFIG, TRUE);
        $file->delete_lines("/^mac\\[.+\\]\s+=\s+$mac/");
    }

    /**
     * Returns agent provision status.
     *
     * @return array agent status information
     * @throws Engine_Exception, Validation_Exception
     */

    public function get_agent_status()
    {
        clearos_profile(__METHOD__, __LINE__);

        $uuid = $this->get_uuid();

        $client = new Rest_Client();

        $response = $client->request($this->api_servers, '/api/v1/deployment/agents/status/' . $uuid);

        if ($response->status_code != 0) {
            $status = self::STATUS_OFFLINE;
        } else if ($response['http_status_code'] == '404') {
            $status = self::STATUS_MISSING;
        } else if ($response['http_status_code'] != '200') {
            $status = self::STATUS_OFFLINE;
        } else {
            // $provisioned = (bool) $response['body']['data']['provisioned'];
            $body = json_decode($response['body']);
            $provisioned = (bool) $body->data->provisioned;

            if ($provisioned)
                $status = self::STATUS_PROVISIONED;
            else
                $status = self::STATUS_UNPROVISIONED;
        }

        return $status;
    }

    /**
     * Returns agent UUID.
     *
     * @return string agent UUID
     * @throws Engine_Exception
     */

    public function get_uuid()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        return $this->uuid;
    }

    /**
     * Returns sink.
     *
     * @return string sink
     * @throws Engine_Exception
     */

    public function get_sink_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        if (!empty($this->config['netifyd']['enable_sink']))
            return $this->config['netifyd']['enable_sink'];
        else
            return FALSE;
    }

    /**
     * Returns MAC whitelist.
     *
     * @return array mac addresses to whitelist
     * @throws Engine_Exception
     */

    public function get_whitelist()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config();

        if (!empty($this->config['filter']['mac']))
            return $this->config['filter']['mac'];
        else
            return [];
    }

    /**
     * Set sink.
     *
     * @param string $sink sink
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_sink_state($sink)
    {
        clearos_profile(__METHOD__, __LINE__);

        if ($sink === 'on' || $sink == 1 || $sink == TRUE)
            $sink = 'yes';
        else
            $sink = 'no';

        $this->_set_parameter('enable_sink', $sink);
    }

    /**
     * Loads configuration.
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _load_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        $config = new File(self::FILE_CONFIG, TRUE);
        $cache = new File(self::FILE_CACHE);

        if (! $cache->exists()) {
            $config->copy_to(self::FILE_CACHE);
            $config->chown('webconfig', 'webconfig');
        }

        $this->config = @parse_ini_file(self::FILE_CACHE, TRUE);

        $cache->delete();

        $file = new File(self::FILE_UUID, TRUE);

        if ($file->exists())
            $this->uuid = trim($file->get_contents());
    }

    /**
     * Generic set routine.
     *
     * @param string $key   key name
     * @param string $value value for the key
     *
     * @return  void
     * @throws Engine_Exception
     */

    protected function _set_parameter($key, $value)
    {
        clearos_profile(__METHOD__, __LINE__);

        try {
            $file = new File(self::FILE_CONFIG, TRUE);
            $match = $file->replace_lines("/^$key\s*=\s*/", "$key = $value\n");

            if (!$match)
                $file->add_lines("$key = $value\n");
        } catch (Exception $e) {
            throw new Engine_Exception(clearos_exception_message($e), CLEAROS_ERROR);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validation routine for sink.
     *
     * @param string $sink sink
     *
     * @return boolean error message if sink is invalid
     */

    public function validate_sink($sink)
    {
        clearos_profile(__METHOD__, __LINE__);
        // Set as 'yes' or 'no', regardless of value
    }
}
