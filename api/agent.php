<?php

/**
 * Netify Agent API controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage rest-api
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\netify\Agent_Library as Agent_Library;
use \clearos\apps\netify\Netifyd as Netifyd;

clearos_load_library('netify/Agent_Library');
clearos_load_library('netify/Netifyd');

// Exceptions
//-----------

use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netify Agent API controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage rest-api
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

class Agent extends ClearOS_REST_Controller
{
    /**
     * Agent status info.
     *
     * @return view
     */

    function index_get()
    {
        $netifyd = new Netifyd();

        try {
            $data['sink_state'] = $netifyd->get_sink_state();
            $data['uuid'] = $netifyd->get_uuid();

            if ($data['sink_state'])
                $data['status'] = $netifyd->get_agent_status();
            else
                $data['status'] = 'not applicable';

            $this->respond_success($data);
        } catch (Exception $e) {
            $this->respond_server_error($e->getMessage());
        }
    }
}
