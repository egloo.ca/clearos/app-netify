<?php

/**
 * Netify controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netify controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016-2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

class Netify extends ClearOS_Controller
{
    /**
     * Netify default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('netify');
        $this->load->library('network/Network');
        $this->load->library('netify/Netifyd');

        // Load view data
        //---------------

        try {
            $is_standalone = $this->network->is_standalone_system();
            $sink_state = $this->netifyd->get_sink_state();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        if ($sink_state)
            $this->page->view_controllers(array('netify/provision'), lang('netify_app_name'));
        else if ($is_standalone)
            $this->page->view_form('netify/mode', [] , lang('netify_app_name'));
        else
            $this->page->view_controllers(array('netify/start'), lang('netify_app_name'));
    }
}
