<?php

/**
 * Netify provision controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netify provision controller.
 *
 * @category   apps
 * @package    netify
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

class Provision extends ClearOS_Controller
{
    /**
     * Index view.
     *
     * @return view
     */

    function index()
    {
        $this->page->view_form('netify/provision', [], lang('base_settings'));
    }

    function start()
    {
        // Load dependencies
        //------------------

        $this->lang->load('netify');
        $this->load->library('netify/Netifyd');

        try {
            $this->netifyd->set_sink_state(TRUE);

            if ($this->netifyd->get_running_state()) {
                $this->netifyd->reset(FALSE);
            } else {
                $this->netifyd->set_boot_state(TRUE);
                $this->netifyd->set_running_state(TRUE);
            }

            $this->page->view_form('netify/provision', [], lang('netify_app_name'));
        } catch (Exception $e) {
            $this->page->view_form('netify/start', [], lang('netify_app_name'));
        }
    }
}
