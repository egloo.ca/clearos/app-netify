<?php

/**
 * Netify network mode warning view.
 *
 * @category   apps
 * @package    netify
 * @subpackage views
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('netify');

$options['buttons'] = array(anchor_custom('/app/network', lang('netify_update_network_mode')));

echo infobox_warning(lang('base_warning'), lang('netify_firewall_mode_warning'), $options);
