<?php

/**
 * Netify provision view.
 *
 * @category   apps
 * @package    netify
 * @subpackage views
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('netify');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo "<div id='netify_sink_enabled'>";

echo form_open('netify/provision');
echo form_header(lang('base_provision'));

echo field_input('uuid', '', lang('netify_provision_code'), TRUE);
echo field_input('provision_status', '', lang('base_status'), TRUE);

echo field_button_set(array(
    anchor_custom('', '', 'high', array('target' => '_blank', 'id' => 'provision_button', 'class' => 'hide'))
));

echo form_footer();
echo form_close();

echo "</div>";
