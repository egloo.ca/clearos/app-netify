<?php

/**
 * Netify start view.
 *
 * @category   apps
 * @package    netify
 * @subpackage views
 * @author     eGloo <team@egloo.ca>
 * @copyright  2018 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/developer
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('netify');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$options['buttons']  = array(
    anchor_custom('/app/netify/provision/start', 'Start Netify', 'high')
);

$blurb = '
    A free trial is available for ClearOS users, so you can start getting insights
    about your network with no obligation.  Start your free trial today!<br><br>
';

echo infobox_highlight('Netify', $blurb, $options);

echo "<div style='width: 50%; margin: 0 auto'>";
echo image('audit-and-forensics.svg');
echo "</div>";
echo "<div style='font-size: 10px; width: 45%; margin: 0 auto'>*Forbes Magazine</div>";
