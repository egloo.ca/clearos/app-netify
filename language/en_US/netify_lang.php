<?php


$lang['netify_active_applications'] = 'Active Applications';
$lang['netify_api_request_error:'] = 'Netify API request error: ';
$lang['netify_api_request_timeout'] = 'Netify API request timeout.';
$lang['netify_app_description'] = 'Netify provides full transparency into what\'s happening on your network.  Features include: device discovery, network intelligence, bandwidth reporting, application detection, protocol analysis, network forensics, and more.';
$lang['netify_application'] = 'Application';
$lang['netify_applications'] = 'Applications';
$lang['netify_app_name'] = 'Netify Agent';
$lang['netify_block_applications'] = 'Block Applications';
$lang['netify_blocked_applications'] = 'Blocked Applications';
$lang['netify_blocked_protocols'] = 'Blocked Protocols';
$lang['netify_block_protocols'] = 'Block Protocols';
$lang['netify_category'] = 'Category';
$lang['netify_dns_issue'] = 'DNS issue detected.';
$lang['netify_mac_address'] = 'MAC Address';
$lang['netify_netify_firewall_agent'] = 'Netify Firewall Agent';
$lang['netify_protocol'] = 'Protocol';
$lang['netify_protocols'] = 'Protocols';
$lang['netify_provision_code'] = 'Provision Code';
$lang['netify_provision'] = 'Provision';
$lang['netify_provisioned'] = 'Provisioned';
$lang['netify_reference'] = 'Reference';
$lang['netify_agent'] = 'Netify Agent';
$lang['netify_tooltip_account_required'] = 'In order to use Netify, you will need to create a %sNetify account%s.';
$lang['netify_tooltip_data'] = 'If the Netify Agent setting is disabled, network analysis will be halted.';
$lang['netify_tooltip_metadata'] = 'The Netify Agent feeds network metadata to the Netify cloud-based service, which in turn, provides an analysis of your network traffic.';
$lang['netify_version'] = 'Agent Version';
$lang['netify_white_list'] = 'White List';
$lang['netify_firewall_mode_warning'] = 'Sorry, Netify can only be run in gateway mode on this system.  If you would like to change your system to <b>Gateway Mode</b>, please click on the button below. Alternatively, please see <a href="https://www.netify.ai/get-netify" target="_blank">the online guide</a> for more ways to deploy Netify on your network.';
$lang['netify_update_network_mode'] = 'Update Network Mode';
$lang['netify_provision_netify_agent'] = 'Provision Netify Agent';
$lang['netify_provision_help'] = 'Blah blah blah';
$lang['netify_provision_code'] = 'Provision Code';
$lang['netify_reprovision'] = 'Reprovision';
$lang['netify_unprovisioned'] = 'Unprovisioned';
$lang['netify_netify_dashboard'] = 'Netify Dashboard';
